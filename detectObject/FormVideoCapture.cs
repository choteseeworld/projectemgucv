﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace detectObject
{
    public partial class FormVideoCapture : Form
    {
        VideoCapture capWebcam;
        bool blnCapturingInProcess = false;

        Mat imgOriginal;
        Image<Bgr, byte> img;
        Image<Gray, byte> imgGrayScale;

        int threshold1, threshold2;

        public FormVideoCapture()
        {
            InitializeComponent();
        }

        private void FormVideoCapture_Load(object sender, EventArgs e)
        {
            try
            {
                capWebcam = new VideoCapture(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("unable to read from webcam, error: " + Environment.NewLine + Environment.NewLine +
                                ex.Message + Environment.NewLine + Environment.NewLine +
                                "exiting program");
                Environment.Exit(0);
                return;
            }
            Application.Idle += processFrameAndUpdateGUI;
            blnCapturingInProcess = true;
        }

        private List<template_img> load_template(string filepath)
        {
            //List<string> st = new List<string> {"bear", "bear_0"};
            string[] st2 =  { "bear", "bear_0" };

            template_img template = new template_img();
            List<template_img> list_template = new List<template_img>();
            int i = 0;

            foreach (string Rank in st2)
            {
                list_template.Add(template);
                list_template[i].name = Rank;
                string filename = Rank + ".jpg";
                list_template[i].img = CvInvoke.Imread(filepath+filename, Emgu.CV.CvEnum.ImreadModes.Grayscale);
                //Console.WriteLine(list_template[0]);
                i++;
            }

            return list_template;

        }

        private void processFrameAndUpdateGUI(object sender, EventArgs e)
        {
            //Read template image
            //string strPath = "C:/Users/sonix/source/repos/detectObject/sources/bear.jpg";
            //string filename = null;
            //filename = Path.GetFileName(strPath);
            //Mat template = CvInvoke.Imread(strPath);
            string strPath = @"C:/Users/sonix/source/repos/detectObject/sources/";
            //Dictionary<Image<Gray, byte>, string> dict_template = 
            //Dictionary<int, Image<Gray, byte>> dict_img = load_train(strPath);
            List<template_img> list_template = load_template(strPath);
            //Console.WriteLine(list_template.Count());

            threshold1 = Convert.ToInt32(trackBar1.Value);
            threshold2 = Convert.ToInt32(trackBar2.Value);
            label3.Text = threshold1.ToString();
            label4.Text = threshold2.ToString();

            imgOriginal = capWebcam.QueryFrame();

            img = imgOriginal.ToImage<Bgr, byte>();
            imgGrayScale = img.Convert<Gray, byte>().ThresholdBinary(new Gray(threshold1), new Gray(threshold2));

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hier = new Mat();

            CvInvoke.FindContours(imgGrayScale, contours, hier, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            
            Dictionary<int, double> dict = new Dictionary<int, double>();

            if (contours.Size > 0)
            {
                for (int i = 0; i < contours.Size; i++)
                {
                    double area = CvInvoke.ContourArea(contours[i]);
                    if (area > 950)
                    {
                        dict.Add(i, area);
                    }
                    //Console.WriteLine("dict: " + dict);
                    Console.WriteLine("Pieces: " + contours.Size);
                }
            }
            var item = dict.OrderByDescending(v => v.Value);    //sorted contours area

            foreach (var it in item)
            {
                int key = int.Parse(it.Key.ToString());

                Rectangle rect = CvInvoke.BoundingRectangle(contours[key]);
                //VectorOfPoint pts = new VectorOfPoint();
                //CvInvoke.ApproxPolyDP(contours[key], pts, CvInvoke.ArcLength(contours[key], true) * 0.005, true);

                //Draw rectangle
                //CvInvoke.Rectangle(imgOriginal, rect, new MCvScalar(0, 0, 255), 2);

                //Find perspective transform from myPerspective function
                Image<Gray, byte> img_mat_Perspective = myPerspective(contours[key], img);

                //Find difference 2 image
                int nonZeroCount = myDiffImage(list_template, img_mat_Perspective);
                //Console.WriteLine("diff: " + nonZeroCount);

                //if (nonZeroCount < 2000)
                //{
                //    //Console.WriteLine("It is: " + filename);
                //    //Console.WriteLine("diff: " + nonZeroCount);
                //    img.Draw(rect, new Bgr(Color.Green), 2);
                //    //CvInvoke.DrawContours(img, contours, -1, new MCvScalar(0,255,0), 1);
                //    //CvInvoke.PutText(img, filename, new Point(rect.X, rect.Y - 5), Emgu.CV.CvEnum.FontFace.HersheySimplex, 0.5, new MCvScalar(0, 255, 0), 1);
                //}

            }

            pictureBox1.Image = img.Bitmap;
            //pictureBox1.Image = imgOriginal.Bitmap;
            pictureBox2.Image = imgGrayScale.Bitmap;
        }

        private int myDiffImage(List<template_img> list_template, Image<Gray, byte> img_mat_Perspective)
        {
            int i = 0;
            int nonZeroCount = 0;
            Dictionary<int, UMat> Dict_diff = new Dictionary<int, UMat>();

            foreach (var it in list_template)
            {
                Image<Gray, byte> templateImage = list_template[i].img.ToImage<Gray, byte>();
                templateImage.ThresholdBinary(new Gray(threshold1), new Gray(threshold2));
                //String s = String.Format("{0}", i);
                //CvInvoke.Imshow("Template Image" + s, templateImage);
                
                UMat result = new UMat(img_mat_Perspective.Rows, img_mat_Perspective.Cols, Emgu.CV.CvEnum.DepthType.Cv32F, 1);

                CvInvoke.AbsDiff(img_mat_Perspective, templateImage, result);
                nonZeroCount = CvInvoke.CountNonZero(result);
                //CvInvoke.Imshow("Diff img", result);

                Dict_diff.Add(nonZeroCount, result);

                i++;
            }

            return nonZeroCount;
        }

        //private int myDiffImage(Image<Gray, byte> templateImage, Image<Gray, byte> img_mat_Perspective, string filename)
        //{
        //    templateImage = templateImage.Convert<Gray, byte>().ThresholdBinary(new Gray(threshold1), new Gray(threshold2));
        //    CvInvoke.Imshow("Template Image", templateImage);

        //    UMat result = new UMat(img_mat_Perspective.Rows, img_mat_Perspective.Cols, Emgu.CV.CvEnum.DepthType.Cv32F, 1);

        //    CvInvoke.AbsDiff(img_mat_Perspective, templateImage, result);
        //    int nonZeroCount = CvInvoke.CountNonZero(result);
        //    CvInvoke.Imshow("Diff img", result);
        //    return nonZeroCount;
        //}

        private Image<Gray, byte> myPerspective(VectorOfPoint contours, Image<Bgr, byte> img)
        {
            Mat data;
            Mat mat_Perspective;
            Image<Gray, byte> img_mat_Perspective;

            RotatedRect minAreaRect = CvInvoke.MinAreaRect(contours);
            PointF[] old_cornor = minAreaRect.GetVertices();
            PointF[] new_cornor = new PointF[4];
            new_cornor[0] = new PointF(0, 0);
            new_cornor[1] = new PointF(0, 100);
            new_cornor[2] = new PointF(100, 100);
            new_cornor[3] = new PointF(100, 0);

            //Draw rotated rectangle
            //img.Draw(minAreaRect, new Bgr(Color.Green), 2);
            //Console.WriteLine(minAreaRect);

            data = CvInvoke.GetPerspectiveTransform(old_cornor, new_cornor);
            mat_Perspective = new Mat();
            CvInvoke.WarpPerspective(img, mat_Perspective, data, new Size(100, 100));

            CvInvoke.CvtColor(mat_Perspective, mat_Perspective, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);

            CvInvoke.Resize(mat_Perspective, mat_Perspective, new Size(0, 0), 2, 2);
            img_mat_Perspective = mat_Perspective.ToImage<Gray, byte>();
            img_mat_Perspective = img_mat_Perspective.Convert<Gray, byte>().ThresholdBinary(new Gray(threshold1), new Gray(threshold2));

            CvInvoke.Imshow("Perspective Image", img_mat_Perspective);

            Mat m = new Mat();

            //img_mat_Perspective.Save("C:/Users/sonix/source/repos/detectObject/sources/bear.jpg");

            //Print key of mat
            //String s = String.Format("Image_Perspective: {0}", key);
            //Console.WriteLine(s);
            //CvInvoke.Imshow(s, img_mat_Perspective);

            return img_mat_Perspective;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (blnCapturingInProcess == true)
            {
                Application.Idle -= processFrameAndUpdateGUI;
                blnCapturingInProcess = false;
                button1.Text = " Resume ";
            }
            else
            {
                Application.Idle += processFrameAndUpdateGUI;
                blnCapturingInProcess = true;
                button1.Text = " Pause ";
            }
        }
    }
}
